stages:
  - build
  - test
  - dast_custom
  - dast_api_custom

include:
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml

.analyzer_run: &analyzer_run
  - apk add jq bash coreutils
  - /analyzer run
  - bash .ci-scripts/$NAME_OF_CI_SCRIPT.sh
  - exit $(eval "$GET_VULNERABILITY_COUNT")

build:
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  stage: build
  image: docker:19.03.12
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  tags:
    - image
  script:
    - docker build -t $IMAGE_NAME .
    - docker push $IMAGE_NAME

dast_custom:
  stage: dast_custom
  image: registry.gitlab.com/gitlab-org/security-products/dast
  variables:
    GET_VULNERABILITY_COUNT: "cat gl-dast-report.json | jq --raw-output '.vulnerabilities | length'"
    NAME_OF_CI_SCRIPT: "dast"
  tags:
    - docker
  allow_failure: true
  artifacts:
    when: always
    paths: [gl-dast-report.json, report.html]
  script:
    - mkdir /zap/wrk/
    - /analyze -t $APP_LINK -r report.html
    - cp /zap/wrk/gl-dast-report.json .
    - cp /zap/wrk/report.html .
    - bash .ci-scripts/$NAME_OF_CI_SCRIPT.sh
    - exit $(eval "$GET_VULNERABILITY_COUNT")

container_scanning:
  stage: test
  variables:
    GET_VULNERABILITY_COUNT: "cat gl-container-scanning-report.json | jq --raw-output '.vulnerabilities | length'"
    CS_SEVERITY_THRESHOLD: "High"
    GIT_STRATEGY: "fetch"
  tags:
    - docker
  rules:
    - when: always
  script:
    - sudo apt-get update
    - sudo apt-get -y install jq
    - gtcs scan
    - exit $(eval "$GET_VULNERABILITY_COUNT")

dependency_scanning_custom:
  stage: test
  variables:
    GET_VULNERABILITY_COUNT: "cat gl-dependency-scanning-report.json | jq --raw-output '.vulnerabilities | length'"
    NAME_OF_CI_SCRIPT: "dependency_scanning"
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium:3
  tags:
    - docker
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  script:
    *analyzer_run

secret_detection:
  variables:
    GIT_STRATEGY: "clone"
    SECURE_LOG_LEVEL: "debug"
    SECRET_DETECTION_HISTORIC_SCAN: "true"
    GET_VULNERABILITY_COUNT: "cat gl-secret-detection-report.json | jq --raw-output '.vulnerabilities | length'"
    NAME_OF_CI_SCRIPT: "secret_detection"
  allow_failure: false
  tags:
    - docker
  script:
    *analyzer_run

dast_api_custom:
  stage: dast_api_custom
  image: owasp/zap2docker-stable
  tags:
    - docker
  artifacts:
    when: always
    paths: [report.html]
  script:
    - mkdir /zap/wrk/
    - cp openapi.json /zap/wrk/openapi.json
    - zap-api-scan.py -t openapi.json -f openapi -r report.html -I -d
    - cp /zap/wrk/report.html .

sonarqube-check:
  stage: test
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  tags:
    - docker
  allow_failure: true