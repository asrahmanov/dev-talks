# Полезные ссылки

## Pre-commit Checks

### gitLeaks

Gitleaks: https://github.com/zricethezav/gitleaks

Конфиг gitLeaks в GitLab: https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml

Описание Secret Detection: https://docs.gitlab.com/ee/user/application_security/secret_detection/

Шаблон Secret Detection: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml

## Commit-time Checks

### SAST

Список анализаторов: https://owasp.org/www-community/Source_Code_Analysis_Tools

Описание SAST в GitLab: https://docs.gitlab.com/ee/user/application_security/sast/

Шаблон SAST: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml

### SonarQube

Установка: https://docs.sonarqube.org/latest/setup/install-server/

Интеграция с GitLab: https://docs.sonarqube.org/latest/analysis/gitlab-integration/

Небольшая инструкция:
1) По первой ссылке установить где-то на сервре SonarQube, например, через docker compose
2) Зайти через веб-интерфейс по порту, указанному в docker-compose.yml
3) Далее перейти по пути Administration -> ALM Integrations -> GitLab, нажимаем кнопку Create configuration.
![.doc/img.png](img.png)
4) Вводим любое название, путь до API гитлаба (https://gitlab-example.com/api/v4), вводим токен пользователя (лучше системного), который имеет доступ к нужным репозиториям. Даём права api, read_api и read_repository
5) Далее на главной странице нажимаем на Add project, выбираем гитлаб 
![.doc/img2.png](img2.png)
6) Выбираем проект и действуем по инструкции (добавляем файл в проект, добавляем ключи в переменные окружения, добавляем задачу в CI/CD)

### Dependency Scanning

Описание в GitLab: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/

### Gemnasium

Репозиторий: https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium

База уязвимостей: https://advisories.gitlab.com/

## Post-build Checks

Описание Container Scanning в GitLab: https://docs.gitlab.com/ee/user/application_security/container_scanning/

Исходники: https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning

Trivy: https://aquasecurity.github.io/trivy/v0.32/tutorials/integrations/gitlab-ci/

## Test-time Checks

DAST в GitLab: https://docs.gitlab.com/ee/user/application_security/dast/

OWASP ZAP: https://www.zaproxy.org/

ZAP - API Scan: https://www.zaproxy.org/docs/docker/api-scan/


## Ещё ссылки

Шаблоны заданий CI/CD тут: Шаблоны CI/CD тут https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates/Jobs

Отчеты гитлаба: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/reports.gitlab-ci.yml

Шаблон автодевопс: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml

Документация: https://gitlab.com/gitlab-org/gitlab/-/tree/8cdb4f0dfa301863a8c1928a1776f01442d7a342/doc/user/application_security

Продукты гитлаба: https://gitlab.com/gitlab-org/security-products

IaC Scanning: https://docs.gitlab.com/ee/user/application_security/iac_scanning/