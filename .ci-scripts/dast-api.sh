#!/bin/bash

vulnerability_count=$(eval "$GET_VULNERABILITY_COUNT")
if [ ${vulnerability_count} -gt 0 ];  then
  echo "|     name     |     severity     |     path     |"
  _jq() {
   echo ${row} | base64 --decode | jq -r ${1}
  }
  for row in $(cat gl-dast-report.json | jq -r '.site.alerts[] | @base64'); do
    echo '|' $(_jq ".name") '|' $(_jq ".severity") '|' $(_jq ".location.path") '|'
  done
fi